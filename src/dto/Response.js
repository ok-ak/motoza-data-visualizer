//@flow
import MotozaLog from "./MotozaLog";

export default class Response {
  data: MotozaLog;
  errors: string[];

  constructor(){
    this.data = new MotozaLog();
    this.errors = [];
  }
}