import Response from '../dto/Response';

export const parserService = {
  _sanitize,
  _removePeriods,
  _removeEmptyRows,
  parse
}

function _sanitize(str: string): string {
  
  return str.replace(/[\n]/g, ";").replace(/[^a-zA-Z\d\,\:\.\-\; ]/g, "").toLowerCase().replace(/\s/g, "_");
}

function _removePeriods(str: string): string {

  return str.replace(/[\.]/g, "");
}

function _removeEmptyRows(arr: string[]): string[] {
  return arr.filter((row) => {
    const result = row.match(/[a-zA-Z\d]/g);

    return result !== null && result.length > 0
  }, []);
}

function parse(str: string): Response {
  const response = new Response();

  if(!str || str === null || str === ""){
    response.errors.push("ParserService Error: Invalid string for parse");

    return response;
  }

  const sanitizedStr: string = _sanitize(str);
  const sanitizedArr: string[] = sanitizedStr.split(';');

  const noEmptyRowArr: string[] = _removeEmptyRows(sanitizedArr);

  const finalArr: string[] = noEmptyRowArr.map((row, index) => {
    if(index === 0){
      const newRow = _removePeriods(row);

      return newRow;
    }

    return row;
  }, []);

  let mapKeys = new Map();
  const keys = finalArr[0].split(',').map((key) => {
    if(!mapKeys.has(key)) {
      mapKeys.set(key, 1);

      return key;
    }
    const value = mapKeys.get(key);
    mapKeys.set(key, value + 1);

    return key + "_" + mapKeys.get(key);
  });

  finalArr.forEach((row, rowIndex) => {
    if(rowIndex !== 0) {
      const elements = row.split(",");
      elements.forEach((el, index) => {
        const key: string = keys[index];
        if(index === 0){
          response.data[key].push(el);
        } else {
          response.data[key].push(Number(el));
        }
      });
    }
  });

  return response;
}