//@flow
import React, { useState } from 'react';
import './App.css';
import HomeScreen from './components/HomeScreen/HomeScreen';
import BottomBanner from './components/BottomBanner/BottomBanner';
import MotozaLog from './dto/MotozaLog';

function App() {

  const [filePath: string, setFilePath: (filePath: string) => void] = useState("");
  const [file: Blob, setFile: (file: Blob) => void] = useState(null);
  const [fileContents: MotozaLog, setFileContents: (fileContents: MotozaLog) => void] = useState(null);

  return (
    <div className="App">
      <div className="App-body">
        <HomeScreen
          filePath = {filePath}
          setFilePath = {setFilePath}
          file = {file}
          setFile = {setFile}
          fileContents = {fileContents}
          setFileContents = {setFileContents}
          />
      </div>
      <div className="App-footer">
        <BottomBanner />
      </div>
    </div>
  );
}

export default App;
