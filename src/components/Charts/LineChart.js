//@flow
import React, { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import { makeStyles } from '@material-ui/core/styles';
import ArrowBackIos from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIos from '@material-ui/icons/ArrowForwardIos';
import './LineChart.css';

const useStyles = makeStyles(theme => ({
  root: {
    height: '70%',
  }
}));

function indexOfMax(arr) {
  if (arr.length === 0) {
      return -1;
  }

  var max = arr[0];
  var maxIndex = 0;

  for (var i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
          maxIndex = i;
          max = arr[i];
      }
  }

  return maxIndex;
}

const getTwoDecimalPlaces = (value: number) => {
  return parseFloat(Math.round(value * 100) / 100).toFixed(2);
}

const LineChart = (props: Props) => {
  const maxMassAirFlowIndex = indexOfMax(props.data['mass_air_flow']);

  const [chartMidIndex, setChartMidIndex] = React.useState(maxMassAirFlowIndex);

  const classes = useStyles();
  const margin = 100;
  const startIndex = (chartMidIndex - margin) < 0? 0 : (chartMidIndex - margin);
  const endIndex = (chartMidIndex + margin) > props.data[props.dataSelection].length - 1?  props.data[props.dataSelection].length - 1 : (chartMidIndex + margin);
  let height;
  let width;


  function handlePreviousChartData(): void {
    const newChartMidIndex = chartMidIndex - margin < 0? 0 : chartMidIndex - margin;
    setChartMidIndex(newChartMidIndex);
  }


  function handleNextChartData(): void {
    const newChartMidIndex = chartMidIndex + margin > props.data[props.dataSelection].length - 1? props.data[props.dataSelection].length - 1 : chartMidIndex + margin;
    setChartMidIndex(newChartMidIndex);
  }

  const buildSample = (dataset: number[]): number[] => {
    console.log('in buildSample', dataset);
    const MAX_DATA_POINTS = 500;
    const stepSize = Math.floor(dataset.length / MAX_DATA_POINTS);
    console.log('stepsize', stepSize)
    return dataset.filter((el, index) => {
      return index % stepSize === 0;
    });
  }

  const overview: number[][] = [
    buildSample(props.data['time']),
    buildSample(props.data[props.dataSelection])
  ];

  // useEffect(() => {
  //   [height, width] = getSize();
  // })

  function getSize() {
    const isClient = typeof window === 'object';
  
    return {
      width: isClient ? window.innerWidth : undefined,
      height: isClient ? window.innerHeight : undefined
    };
  }

  const overviewData = {
    labels: overview[0],
    datasets: [{
      label: [],
      fill: true,
      lineTension: 0.1,
      backgroundColor: 'rgb(142, 142, 142, 0.4)',
      borderColor: 'rgba(140, 140, 140, 0.6)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(140, 140, 140, 0.8)',
      pointBackgroundColor: 'rgba(140, 140, 140, 0.8)',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: overview[1]
    }]
  };

  let labels = ['time', props.dataSelection]
  // let labels = Object.keys(props.data);
  const time = labels.shift();

  const data = {
    labels: props.data[time].slice(startIndex, endIndex),
    datasets: labels.map((set, index) => {
      return {
        label: set,
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: props.data[set].slice(startIndex, endIndex)
      }
    })
  };

  console.log(props.data)

  return (
    <div className={classes.root}>
      <div className='header-container'>
        <div className='header'>
          <h2>Your Log Data</h2>
          <h4>Estimated Peak Engine HP ~ {getTwoDecimalPlaces(props.data['mass_air_flow'][maxMassAirFlowIndex] * 1.25)}</h4>
          <h4>Estimated Peak Wheel HP ~ {getTwoDecimalPlaces(props.data['mass_air_flow'][maxMassAirFlowIndex] * 1.25 * (100 - 14)/100)}</h4>
        </div>
        <h3>Overview</h3>
        <div className='overview'>
          <Line
                data={overviewData}
                width={100}
                height={20}
                legend={{
                  display: false
                }}
                options={{
                  maintainAspectRatio: false,
                  scales: {
                    xAxes: [{
                      ticks: {
                        display: false
                      }
                    }],
                    yAxes: [{
                      ticks: {
                        display: false
                      }
                    }],
                    tooltips: {
                      enabled: false
                    }
                  }
                }}/>
        </div>
        <h3>Zoomed View</h3>
      </div>
      <div className='chart-container'>
        <div
          className='chart-nav-button'
          onClick={handlePreviousChartData}>
          <ArrowBackIos/>
        </div>
        <div className = 'chart-content'>
          <Line
              data={data}
              width={100}
              height={100}
              options={{ maintainAspectRatio: false }}/>
        </div>
        <div
          className='chart-nav-button'
          onClick={handleNextChartData}>
          <ArrowForwardIos/>
        </div>
      </div>
    </div>
  )
}

export default LineChart;