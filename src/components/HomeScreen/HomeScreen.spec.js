//@flow
import React from 'react';
import ReactDOM from 'react-dom';
import { mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { until, createShallow } from '@material-ui/core/test-utils';
import HomeScreen from './HomeScreen';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';

describe('HomeScreen', () => {
  it('has file input button', () => {
    //when
    const shallow = createShallow()
    const component = shallow(
      <HomeScreen />
    );

    const buttonComponent = component.find('#HomeScreen-button-select-file');

    //then
    expect(buttonComponent.text()).toEqual('Select File');
  });

  it('has file path input text field that is disabled and initially blank', () => {
    //when
    const shallow = createShallow()
    const component = shallow(
      <HomeScreen />
    );

    const textAreaComponent = component.find('#HomeScreen-text-field-file-path');

    //then
    expect(textAreaComponent.text()).toEqual('');
    expect(textAreaComponent.prop('disabled')).toEqual(true);
  });

  it('should have a button with a passed buttonText prop "Visualize"', () => {
    //when
    const shallow = createShallow()
    const component = shallow(
      <HomeScreen />
    );

    const buttonComponent = component.find('#FullScreenDialog-button-visualize');

    //then
    expect(buttonComponent.prop('buttonText')).toEqual('Visualize');
  });
});


