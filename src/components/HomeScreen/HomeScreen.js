//@flow
import React, { useState } from 'react';
import Paper from '@material-ui/core/Paper';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import './HomeScreen.css';
import { parserService } from '../../services/parserService';
import FullScreenDialog from '../FullScreenDialog/FullScreenDialog';
import MotozaLog from '../../dto/MotozaLog';
import Response from '../../dto/Response';
import Fab from '@material-ui/core/Fab';

type Props = {
  filePath: string,
  setFilePath: (filePath: string) => void,
  file: Blob,
  setFile: (file: Blob) => void,
  fileContents: MotozaLog,
  setFileContents: (fileContents: MotozaLog) => void
}

let exampleFileContents: string = "";

function buildExampleFileContents(exampleFile: Blob) : void {
  const encoding = 'utf-8';
  const reader = new FileReader();

  console.log("!!!!!!!!!!!!", exampleFile);
  console.log("!!!!!!!!!!!!", exampleFile.type);
  reader.readAsText(exampleFile, encoding);
  
  let parsedResult = "";
  reader.onloadend = async function(e) {
    const rawText = await reader.result;
    const response: Response = await parserService.parse(rawText);
    exampleFileContents = response.data;
  }
}

const HomeScreen = (props: Props) => {
 
  const selectFileHandler = (event: Event) => {
    props.setFilePath(event.target.value);
    props.setFile(event.target.files[0]);
    // (event) => setFile (event.target.file)
  }

  const submitHandler = (file, callback) => {
    callback(file);
    // console.log(fileContents);
  }
      
  const buildFileContents = (csvFile: Blob) => {
    const encoding = 'utf-8';
    const reader = new FileReader();

    reader.readAsText(csvFile, encoding);
    
    let parsedResult = "";
    reader.onloadend = async function(e) {
      const rawText = await reader.result;
      const response: Response = await parserService.parse(rawText);
      props.setFileContents(response.data);
      // console.log(response.data);
    }
  }

  const hasData = () => {
    if(!props.filePath || props.filePath === null || props.filePath === ""){
      return false;
    }
    return true;
  }

  return(
    <div className="HomeScreen-container">
      <h1>Motoza Log Visualizer</h1>
      <Paper className="HomeScreen-paper">
        <div className="HomeScreen-input-row">
          <input
            accept=".csv"
            className="HomeScreen-input"
            id="logFile"
            single="true"
            type="file"
            onChange={selectFileHandler}
          />
          <label htmlFor="logFile">
            <Button
              id="HomeScreen-button-select-file"
              component="span"
              variant="contained">
              Select File
            </Button>
          </label>
        </div>
        <div className="HomeScreen-input-row">
          <TextField
            id="HomeScreen-text-field-file-path"
            disabled
            value={props.filePath}
            margin="normal"
            variant="outlined"
            style={{display: "tableCell", width: "100%", margin: "8px"}}
          />
        </div>
        <div className="HomeScreen-input-row">
          <FullScreenDialog
            id="FullScreenDialog-button-visualize"
            data={props.fileContents}
            hasData={hasData()}
            submitHandler={() => submitHandler(props.file, buildFileContents)}
            buttonText="Visualize" />
        </div>
      </Paper>
    </div>
  )

}

export default HomeScreen;